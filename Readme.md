## Índice
- [Tecnologías usadas](#tecnologías-usadas)
- [Pasos para levantar los servicios](#pasos-para-levantar-los-servicios)
  - [Instalar e iniciar minikube](#1-instalar-e-iniciar-minikube)
  - [Agregar el repositorio de Helm al cluster](#2-agregar-el-repositorio-de-helm-al-cluster)
  - [Instalar el chart de Helm en el clúster](#3-instalar-el-chart-de-helm-en-el-clúster)
  - [Desplegar la API](#4-desplegar-la-api)
  - [Iniciar el tunel para exponer los ingress al entorno local](#5-iniciar-el-tunel-para-exponer-los-ingress-al-entorno-local)
- [Subir imagen al registry de gitlab](#subir-imagen-al-registry-de-gitlab)
- [Comandos adicionales](#comandos-adicionales)

---
# Tecnologías usadas
* **React**: libreria para el desarrollo del frontend.
* **Javalin**: framework ligero de desarrollo web para Java.
* **CouchDB**: base de datos NoSQL basada en documentos.
* **Kubernetes**: plataforma de orquestación de contenedores.
* **Helm**: herramienta de administración de paquetes para Kubernetes. Usado para instalar el chart de CouchDB.
* **Docker**: usado como el controlador de virtualización para montar el nodo de minikube.
---

# Pasos para levantar los servicios
### 1. Instalar e iniciar minikube
Instalar segun el SO desde https://minikube.sigs.k8s.io/docs/start/.
```
//Iniciar nodo
minikube start --profile=cloud-computing --memory=2048 --cpus=2 --disk-size=5g --kubernetes-version=v1.24.6 --driver=docker --listen-address=0.0.0.0

//Habilitar el controlador de ingress nginx en el cluster
minikube addons enable ingress

//Iniciar nodo de minikube
minikube start

//Detener nodo
minikube stop
```

### 2. Agregar el repositorio de Helm al cluster
```
helm repo add couchdb https://apache.github.io/couchdb-helm
```

### 3. Instalar el chart de Helm en el clúster
```
helm install cluster-couchdb --version=4.4.0 --set couchdbConfig.couchdb.uuid=decafbaddecafbaddecafbaddecafbad --set ingress.enabled=true --set ingress.hosts[0].host=localhost couchdb/couchdb
```

### 4. Desplegar la API
```
//Esto desplegara el deployment, service e ingress
kubectl apply -f .\deploy-api.yaml
```
### 5. Iniciar el tunel para exponer los ingress al entorno local
```
minikube tunnel
```
---
# Subir imagen al registry de gitlab
```
// Loguearse
docker login registry.gitlab.com

// Construir la imagen con el dockerfile
docker build -t registry.gitlab.com/rodrigo906/cluster-couchdb .

//Subir la imagen al registry
docker push registry.gitlab.com/rodrigo906/cluster-couchdb
```
---
# Comandos adicionales
```
// Eliminar el helm desplegado en el cluster
helm delete cluster-couchdb

//Acceder a la interfaz grafica de minikube
minikube dashboard

//Hacer un port forward
kubectl port-forward <nombre-del-pod> <puerto-local>:<puerto-del-pod>
```


