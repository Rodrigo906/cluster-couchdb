FROM openjdk:11.0.11-jre-slim
COPY api-env.jar api.jar
CMD java -jar api.jar
EXPOSE 7004